from django.contrib import admin

from .models import Question, Choice


#  class QuestionAdmin(admin.ModelAdmin):
#    fields = ['pub_date' , 'question_text']

class ChoiceInLine(admin.StackedInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [

        ('Date information', {'fields': ['pub_date']}),
        (None, {'fields': ['question_text']}),
    ]

    inlines = [ChoiceInLine]


admin.site.register(Question, QuestionAdmin)
